# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023, 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_lookandfeel_org.kde.lookandfeel\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 21:10+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "פריסת מקלדת: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "שם משתמש"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "סיסמה"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "כניסה"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Caps Lock דולק"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "שינה"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "הפעלה מחדש"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "כיבוי"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "אחר…"

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "נא להקליד שם משתמש וסיסמה"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "הצגת משתמשים"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "מקלדת וירטואלית"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "הכניסה נכשלה"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "הפעלת שולחן עבודה: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "שעון:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "להשאיר גלוי כשבקשת השחרור נעלמת"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "פקדי מדיה:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "הצגה תחת בקשת השחרור"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "השחרור נכשל"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "שינה"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "תרדמת"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "החלפת משתמש"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "החלפת פריסת מקלדת"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "פתיחה"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(או לסרוק את האצבע שלך על הקורא)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(או לסרוק את הכרטיס החכם שלך)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "אין כותרת"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "לא מתנגנת מדיה"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "הרצועה הקודמת"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "נגינה או השהיה של מדיה"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "הרצועה הבאה"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "התקנת עדכוני תוכנה והפעלה מחדש בעוד שנייה"
msgstr[1] "התקנת עדכוני תוכנה והפעלה מחדש בעוד שתי שניות"
msgstr[2] "התקנת עדכוני תוכנה והפעלה מחדש בעוד %1 שניות"
msgstr[3] "התקנת עדכוני תוכנה והפעלה מחדש בעוד %1 שניות"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "מופעל מחדש עוד שנייה"
msgstr[1] "מופעל מחדש עוד שתי שניות"
msgstr[2] "מופעל מחדש עוד %1 שניות"
msgstr[3] "מופעל מחדש עוד %1 שניות"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "יציאה מהפעלה עוד שנייה"
msgstr[1] "יציאה מהפעלה עוד שתי שניות"
msgstr[2] "יציאה מהפעלה עוד %1 שניות"
msgstr[3] "יציאה מהפעלה עוד %1 שניות"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "נכבה עוד שנייה"
msgstr[1] "נכבה עוד שתי שניות"
msgstr[2] "נכבה עוד %1 שניות"
msgstr[3] "נכבה עוד %1 שניות"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"יש עוד משתמש במערכת. אם המחשב יכבה או יופעל מחדש זה עלול להוביל לאובדן מידע "
"שלו."
msgstr[1] ""
"יש עוד שני משתמשים במערכת. אם המחשב יכבה או יופעל מחדש זה עלול להוביל לאובדן "
"מידע שלהם."
msgstr[2] ""
"יש עוד %1 משתמשים במערכת. אם המחשב יכבה או יופעל מחדש זה עלול להוביל לאובדן "
"מידע שלהם."
msgstr[3] ""
"יש עוד %1 משתמשים במערכת. אם המחשב יכבה או יופעל מחדש זה עלול להוביל לאובדן "
"מידע שלהם."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "לאחר הפעלה מחדש, המחשב ייכנס למסך הגדרת הקושחה."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "שינה עכשיו"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "תרדמת עכשיו"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "התקנת &עדכונים והפעלה מחדש"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "התקנת &עדכונים והפעלה מחדש עכשיו"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "הפעלה מחדש עכשיו"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "כיבוי עכשיו"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "יציאה"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "יציאה עכשיו"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "ביטול"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "פלזמה פותחה על ידי KDE"

#~ msgid "OK"
#~ msgstr "אישור"

#~ msgid "Switch to This Session"
#~ msgstr "מעבר להפעלה זו"

#~ msgid "Start New Session"
#~ msgstr "התחלת הפעלה חדשה"

#~ msgid "Back"
#~ msgstr "חזרה"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "הסוללה טעונה ב־%1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "לא בשימוש"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "ב־TTY‏ %1 (תצוגה %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "Configure"
#~ msgstr "הגדרות"

#, fuzzy
#~| msgid "Configure"
#~ msgid "Configure KRunner Behavior"
#~ msgstr "הגדרות"

#, fuzzy
#~| msgid "Configure"
#~ msgid "Configure KRunner…"
#~ msgstr "הגדרות"

#, fuzzy
#~| msgctxt "Textfield placeholder text, query specific KRunner"
#~| msgid "Search '%1'..."
#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "חיפוש \"%1\"..."

#, fuzzy
#~| msgctxt "Textfield placeholder text"
#~| msgid "Search..."
#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "חיפוש..."

#, fuzzy
#~| msgid "Close Search"
#~ msgid "Pin Search"
#~ msgstr "סגור חיפוש"

#~ msgid "Recent Queries"
#~ msgstr "חיפושים אחרונים"

#~ msgid "Remove"
#~ msgstr "הסר"

#, fuzzy
#~| msgid "Recent Queries"
#~ msgid "in category recent queries"
#~ msgstr "חיפושים אחרונים"

#~ msgid "Configure Search Plugins"
#~ msgstr "הגדר תוסף חיפוש"

#~ msgid "Close"
#~ msgstr "סגור"

#~ msgid "Suspend"
#~ msgstr "השהה"

#~ msgid "Reboot"
#~ msgstr "הפעל מחדש"

#~ msgid "Logout"
#~ msgstr "התנתק"

#~ msgid "Different User"
#~ msgstr "משתמש אחר"

#, fuzzy
#~| msgid "Login as different user"
#~ msgid "Log in as a different user"
#~ msgstr "התחבר כמשתמש אחר"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "סיסמה"

#~ msgid "Login"
#~ msgstr "התחברות"

#~ msgid "Switch"
#~ msgstr "החלף"
