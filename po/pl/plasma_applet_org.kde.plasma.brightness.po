# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmai.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-02 00:39+0000\n"
"PO-Revision-Date: 2023-12-23 11:30+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmai.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 23.08.3\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Brightness and Color"
msgstr "Jasność i barwa"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Screen brightness at %1%"
msgstr "jasność ekranu przy %1%"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "jasność klawiatury przy %1%"

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Nocne światło wyłączone"

#: package/contents/ui/main.qml:85
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Nocne światło przy %1K"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Przewiń, aby dostosować jasność ekranu"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Kliknij środkowym, aby przełączyć nocne światło"

#: package/contents/ui/main.qml:202
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Ustawienia nocnego światła…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "Wył."

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Niedostępne"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Niewłączone"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Nieuruchomione"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Wł."

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Przejście o poranku"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "Dzień"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Przejście wieczorem"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Noc"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "Ustawienia…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "Włącz i ustaw…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Przejście na dzień kończące się o:"

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Przejście na noc zaplanowane na:"

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Przejście na noc kończące się o:"

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Przejście na dzień zaplanowane na:"

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "Jasność wyświetlacza"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "Jasność klawiatury"

#: package/contents/ui/PopupDialog.qml:128
#, kde-format
msgid "Night Light"
msgstr "Nocne światło"
